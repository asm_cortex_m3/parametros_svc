		.syntax		unified
		.cpu		cortex-m3
		.thumb

		.section	.text
		.align		2
 		.global		main, SVC_Handler


/****************************************/
/*	Service CALL de ejemplo. 			*/
/*  		                            */
/****************************************/
.type		SVC_Handler, %function
SVC_Handler:
        //Saco el parámetro de la SVC como hace
        //Yiu  en el capítulo 10 del libro de Cortex M3 y M4.
        TST     LR, #4              //Reviso el BIT 2 de EXC_RETURN
        ITE     EQ                  //Bloque condicional de 2 instrucciones.
        MRSEQ   R0, MSP             //Sí es cero (Z=1) vengo del MSP, lo copio a R0
        MRSNE   R0, PSP             //Sí es uno  (Z=0) vengo del PSP, lo copio a R0
        LDR     R0, [R0, #24]       //Busco el PC del stacking.
        LDRB    R0, [R0, #-2]       //De la instrucción apuntada por R0 me voy a la anterior (SVC)
        AND     R0, R0, #0x01       //SVC y me traigo el parámetro y le hago la and.
        MRS     R1, CONTROL         //Copio el registro de control en R1.
        BFI     R1,R0,0,1           //Pongo el bit 0 de R0 en el bit 0 de R1
        MSR     CONTROL, R1         //Recargo el registro de control
        BX      LR                  //Vuelvo.
.end
